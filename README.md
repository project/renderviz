# Render Visualization

This module makes the cache properties of Drupal's render array output
accessible by viewing them.

See https://wimleers.com/blog/renderviz-prototype.

**Console interface**

The only interface to the module is the developer console of your browser. Help
text is displayed in the console when you refresh the page. The following
commands are available:

```js
// Query for render data.
renderviz(metadataType, metadataValue)

// Focus one result from the query result set.
rendervizFocus(index)
```

Usage examples:
```js
// Select all elements that have a 'timezone' context.
renderviz('contexts', 'timezone')

// Select all elements that have the 'node:1' tag.
renderviz('tags', 'node:1')

// Select all elements that have infinite cache life time.
renderviz('max-age', '-1')

// Set focus on the first element found with renderviz().
rendervizFocus(0)

// Move focus to the second element.
rendervizFocus(1)
```


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.
