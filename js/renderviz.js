/* eslint-disable no-console */
(($, JSON, Drupal, once) => {
  // Map the metadata to data- attributes.
  function setMetadata(commentNode, element) {
    const metadata = JSON.parse(commentNode.textContent);
    Object.keys(metadata).forEach((key) => {
      let value = metadata[key];
      if (value instanceof Array) {
        if (value.length === 0) {
          return;
        }
        value = value.join(' ');
      }
      element.setAttribute(`data-renderviz-${key}`, value);
    });
  }

  // Map the pre-bubbling metadata to data- attributes.
  function setPrebubblingMetadata(commentNode, element) {
    const prebubblingMetadata = JSON.parse(commentNode.textContent);
    Object.keys(prebubblingMetadata).forEach((key) => {
      let value = prebubblingMetadata[key];
      if (value instanceof Array) {
        if (value.length === 0) {
          return;
        }
        value = value.join(' ');
      }
      element.setAttribute(`data-renderviz-prebubbling-${key}`, value);
    });
  }

  function visualize(queryMetadataType, queryMetadataValue) {
    // One-time initialization.
    $(once('renderviz', 'html')).addClass('renderviz');
    // Reset.
    $('.renderviz-trace-layer').removeClass('renderviz-trace-layer');
    $('.renderviz-trace-layer-root').removeClass('renderviz-trace-layer-root');
    $('.renderviz-trace-focus').removeClass('renderviz-trace-focus');
    // Apply the new query.
    const result = $(
      `[data-renderviz-${queryMetadataType}~="${queryMetadataValue}"]`,
    );
    console.info(`${result.length} matches found.`);
    console.info(result);
    result.addClass('renderviz-trace-layer');
    $(
      `[data-renderviz-prebubbling-${queryMetadataType}~="${queryMetadataValue}"]`,
    ).addClass('renderviz-trace-layer-root');
    window.rendervizLastType = queryMetadataType;
    window.rendervizLastValue = queryMetadataValue;
  }

  function focus(index) {
    // Reset.
    $('.renderviz-trace-focus').removeClass('renderviz-trace-focus');
    // Apply the new focus.
    const el = $(
      `[data-renderviz-${window.rendervizLastType}~="${window.rendervizLastValue}"]`,
    )[index];
    console.info(el);
    $(el).addClass('renderviz-trace-focus');
  }

  function removeDuplicates(arr) {
    return arr.filter((item, index) => {
      return arr.indexOf(item) === index;
    });
  }

  Drupal.behaviors.renderviz = {
    attach(context) {
      // Transplant the data from the HTML comments onto the parent element.
      const comments = $(context).comments(true);
      for (let i = 0; i < comments.length; i++) {
        if (comments[i].textContent === 'RENDERER_START') {
          let element = comments[i].nextElementSibling;
          if (element) {
            // Mark the element for renderviz treatment.
            element.setAttribute('data-renderviz-element', true);
            setMetadata(comments[i + 1], element);
            setPrebubblingMetadata(comments[i + 2], element);
          }
          // @todo improve this; might need some complex merging logic.
          // If we have renderer metadata, but it's not for an Element node,
          // then it is for a Text node. In that case, set the pre-bubbling
          // metadata of the Text node on the parent Element node.
          // e.g. TimestampFormatter — the node timestamp
          else {
            element = comments[i].parentElement;
            setPrebubblingMetadata(comments[i + 2], element);
          }
        }
      }

      once('renderviz-init', 'body', context).forEach(() => {
        let contexts = [];
        let tags = [];
        $('[data-renderviz-contexts]').each((index, element) => {
          contexts = contexts.concat(
            element.attributes['data-renderviz-contexts'].value.split(' '),
          );
        });
        contexts = removeDuplicates(contexts);
        $('[data-renderviz-tags]').each((index, element) => {
          tags = tags.concat(
            element.attributes['data-renderviz-tags'].value.split(' '),
          );
        });
        tags = removeDuplicates(tags);
        console.info(
          `${
            $('[data-renderviz-element]').length
          } unique rendered elements on the page.`,
          '\nContexts:',
          contexts,
          '\nTags:',
          tags,
        ); // eslint-disable-line quotes
        console.info(
          "To use:\n- Querying: `renderviz(metadataType, metadataValue)`, e.g. `renderviz('contexts', 'timezone')`.\n- Focusing: `rendervizFocus(index)`, e.g. `rendervizFocus(0)` to focus on the first element of the last query.",
        ); // eslint-disable-line quotes
        window.renderviz = visualize;
        window.rendervizFocus = focus;
      });
    },
  };
})(jQuery, window.JSON, Drupal, once);
